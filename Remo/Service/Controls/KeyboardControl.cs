﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WindowsInput;

namespace Remo.Service.Controls
{
    [DisplayName("Keyboard"), Description("Simulate keyboard presses for varying keys"), ControlType(ControlType.Action)]
    class KeyboardControl : Control
    {
        private IInputSimulator inputSimulator;

        public KeyboardControl() : base() {
            inputSimulator = new InputSimulator();
        }

        public override byte[] Get()
        {
            return EmptyValue;
        }

        public override void Set(byte[] value, Device setter)
        {
            base.Set(value, setter);
            if (value.Length == 0)
                return;
            byte keyValue = value[0];
            WindowsInput.Native.VirtualKeyCode keyCode = (WindowsInput.Native.VirtualKeyCode)keyValue;
            inputSimulator.Keyboard.KeyPress(keyCode);
        }
    }
}
