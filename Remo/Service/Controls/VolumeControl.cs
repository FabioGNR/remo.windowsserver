﻿using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace Remo.Service.Controls
{
    [DisplayName("Volume"), Description("Control the volume of the hosting device"), ControlType(ControlType.NumericValue)]
    class VolumeControl : Control, IDisposable
    {
        static MMDevice DefaultDevice;
        static MMDeviceEnumerator enumerator;
        static VolumeNotifyer notifyer;
        private static ThreadWorker worker;
        private ushort value = 0;

        public VolumeControl()
        {
            worker = new ThreadWorker("Volume");
            worker.Execute(InitializeDependencies);
        }

        public override void Stop()
        {
            worker.Stop();
        }

        private void InitializeDependencies()
        {
            enumerator = new MMDeviceEnumerator();
            DefaultDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console | Role.Multimedia);
            notifyer = new VolumeNotifyer();
            enumerator.RegisterEndpointNotificationCallback(notifyer);
        }

        private void UpdateValue()
        {
            value = (ushort)(DefaultDevice.AudioEndpointVolume.MasterVolumeLevelScalar * 100f);
        }

        class VolumeNotifyer : NAudio.CoreAudioApi.Interfaces.IMMNotificationClient
        {
            public void OnDefaultDeviceChanged(DataFlow flow, Role role, string ID)
            {
                if (role != Role.Console && role != Role.Multimedia) return;
                if (ID != null)
                {
                    MMDeviceCollection a = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
                    MMDevice NewDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console | Role.Multimedia);
                    DefaultDevice = NewDevice;
                }
                else
                    DefaultDevice = null;
            }

            public void OnDeviceAdded(string ID) { }
            public void OnDeviceRemoved(string ID) { }
            public void OnDeviceStateChanged(string ID, DeviceState State) { }
            public void OnPropertyValueChanged(string ID, PropertyKey Key) { }
        }

        public override byte[] Get()
        {
            worker.Execute(UpdateValue);
            return BitConverter.GetBytes(value);
        }

        public override void Set(byte[] value, Device setter)
        {
            ushort valueShort = BitConverter.ToUInt16(value, 0);
            base.Set(value, setter);
            float level = valueShort / 100f;
            worker.Execute(() => DefaultDevice.AudioEndpointVolume.MasterVolumeLevelScalar = level);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    enumerator.Dispose();
                }

                worker.Stop();
                worker = null;

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~VolumeControl() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
