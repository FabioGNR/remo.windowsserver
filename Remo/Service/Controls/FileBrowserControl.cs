﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Remo.Service.Controls
{
    [DisplayName("File Browser"), Description("Browse files and open them"), ControlType(ControlType.Unknown)]
    class FileBrowserControl : Control
    {
        string defaultPath;
        string currentPath;

        public FileBrowserControl() : base()
        {
            Properties.Settings.Default.PropertyChanged += Default_PropertyChanged;
            LoadDefaultPath();
            currentPath = defaultPath;
        }

        private void LoadDefaultPath()
        {
            defaultPath = Properties.Settings.Default.FileBrowserDefaultPath;
            if (string.IsNullOrWhiteSpace(defaultPath))
                defaultPath = "C:\\";
        }

        private void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Properties.Settings.Default.FileBrowserDefaultPath))
            {
                LoadDefaultPath();
            }
        }

        private string GetValue()
        {
            DirectoryInfo directory = new DirectoryInfo(currentPath);
            if (directory.Exists)
            {
                StringBuilder responseBuilder = new StringBuilder();
                if (directory.Parent != null)
                {
                    responseBuilder.AppendLine(directory.Parent.FullName);
                }
                foreach (var dir in directory.EnumerateDirectories())
                {
                    responseBuilder.AppendLine(dir.FullName);
                }
                foreach (var file in directory.EnumerateFiles())
                {
                    responseBuilder.AppendLine(file.FullName);
                }
                return responseBuilder.ToString();
            }
            return string.Empty;
        }

        public override byte[] Get()
        {
            string value = GetValue();
            return Encoding.UTF8.GetBytes(value);
        }

        public override void Set(byte[] value, Device setter)
        {
            base.Set(value, setter);
            string path = Encoding.UTF8.GetString(value);
            if (path.Length == 0)
            {
                // reset
                currentPath = defaultPath;
            }
            else if (!string.IsNullOrWhiteSpace(path))
            {
                // go to directory or execute file
                if (Directory.Exists(path))
                {
                    currentPath = path;
                }
                else if (File.Exists(path))
                {
                    // start process
                    var process = Process.Start(path);
                    // attempt to focus window
                    var windowHandle = process.MainWindowHandle;
                    if (windowHandle != IntPtr.Zero)
                    {
                        NativeMethods.ShowWindow(windowHandle, 5); // 5 = SW_SHOW, 9 = SW_RESTORE
                    }
                }
            }
        }
    }
}
