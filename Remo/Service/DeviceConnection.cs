﻿using System;
using System.Net.Sockets;
using System.Threading;
using Remo.Service.Protocol;
using Google.Protobuf;

namespace Remo.Service
{
    public enum ConnectionState
    {
        Foreign, Connected, Disconnected
    }

    public class DeviceConnection
    {
        private readonly Device Device;
        private bool run = false;

        public delegate void AuthenticatedHandler(object sender, AuthenticatedEventArgs e);
        public event AuthenticatedHandler Authenticated;
        public delegate void DisconnectedHandler(object sender, DeviceEventArgs e);
        public event DisconnectedHandler Disconnected;

        public Thread ConnectionThread { get; set; }
        public TcpClient Client { get; set; }
        public ConnectionState State { get; set; }
        public DeviceConnection(TcpClient client, Device device)
        {
            Client = client;
            Device = device;
            State = ConnectionState.Foreign;
            run = true;
            ConnectionThread = new Thread(() => Authenticate())
            {
                Name = $"Device {Device.Identifier} Connection Thread"
            };
            ConnectionThread.Start();
        }

        public void Disconnect()
        {
            run = false;
        }

        private void OnDisconnect()
        {
            State = ConnectionState.Disconnected;
            Disconnected?.Invoke(this, new DeviceEventArgs(Device));
        }

        void Interact()
        {
            while(run)
            {
                if (!Client.Connected)
                    break;

                if (Client.Available > 0)
                {
                    ConnectedMessage msg = Client.ReadMessage<ConnectedMessage>();
                    if(msg.MessagesCase != ConnectedMessage.MessagesOneofCase.None)
                        ProcessMessage(msg, Device);
                }
                else
                    Thread.Sleep(100);
            }
            OnDisconnect();
            Client.Close();
        }

        void ReturnControlValue(string controlName)
        {
            byte[] valueBytes = Controller.Instance.GetControlValue(controlName);
            var returnControlValue = new ReturnControlValue()
            {
                ControlName = controlName,
                Value = ByteString.CopyFrom(valueBytes)
            };
            var response = new ConnectedMessage() { ReturnControlValue = returnControlValue };
            Client.WriteMessage(response);
        }

        void ProcessMessage(ConnectedMessage msg, Device source)
        {
            try
            {
                switch (msg.MessagesCase)
                {
                    case ConnectedMessage.MessagesOneofCase.SetControlValue:
                        byte[] value = msg.SetControlValue.Value.ToByteArray();
                        Controller.Instance.SetControlValue(msg.SetControlValue.ControlName, value, source);
                        ReturnControlValue(msg.SetControlValue.ControlName);
                        break;
                    case ConnectedMessage.MessagesOneofCase.GetControlValue:
                        ReturnControlValue(msg.GetControlValue.ControlName);
                        break;
                    case ConnectedMessage.MessagesOneofCase.PingPong:
                        var response = new ConnectedMessage
                        {
                            PingPong = new PingPong()
                        };
                        Client.WriteMessage(response);
                        break;
                }
            }
            catch(Exception)
            {
                return;
            }

        }

        void ProcessAuthenticationMessage(AuthenticationMessage msg)
        {
            switch (msg.MessagesCase)
            {
                case AuthenticationMessage.MessagesOneofCase.Authenticate:
                    AuthenticationResult result = new AuthenticationResult();
                    if (ConnectionManager.Authenticate(msg.Authenticate.Token))
                    {
                        State = ConnectionState.Connected;
                        Authenticated?.Invoke(this, new AuthenticatedEventArgs(msg.Authenticate.Identifier));
                        result.Success = true;
                    }
                    else
                        result.ErrorMessage = "Token was not authorized";
                    AuthenticationMessage response = new AuthenticationMessage()
                    {
                        AuthenticationResult = result
                    };
                    Client.WriteMessage(response);
                    break;
            }
        }

        void Authenticate()
        {
            bool requested = false;
            while (State == ConnectionState.Foreign && run)
            {
                if (Client.Available > 0)
                {
                    var msg = Client.ReadMessage<AuthenticationMessage>();
                    if (msg.MessagesCase != AuthenticationMessage.MessagesOneofCase.None)
                    {
                        ProcessAuthenticationMessage(msg);
                    }
                    else
                        requested = false;
                }
                else if (!requested)
                {
                    Client.WriteMessage(new RequestAuthentication());
                    requested = true;
                }
                Thread.Sleep(100);
            }
            Interact();
        }
    }
}
