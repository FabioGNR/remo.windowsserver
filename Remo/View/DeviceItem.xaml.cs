﻿using Remo.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Remo.View
{
    /// <summary>
    /// Interaction logic for DeviceItem.xaml
    /// </summary>
    public partial class DeviceItem : UserControl
    {
        public Device Device { get; set; }
        public DeviceItem(Device device)
        {
            InitializeComponent();
            Device = device;
            device.Connection.Disconnected += Connection_Disconnected;
            if (Device.Connection.State == ConnectionState.Foreign)
            {
                Device.Connection.Authenticated += Connection_Authenticated;
                deviceNameText.Text = "Authenticating...";
            }
            else
            {
                SetDeviceNameText(Device.Identifier);
            }
            connectTimeText.Text = string.Format("Connected at {0}",
            DateTime.Now.ToShortTimeString());
        }

        private void Connection_Disconnected(object sender, EventArgs e)
        {
            if (!Dispatcher.HasShutdownStarted)
            {
                Dispatcher.Invoke(RemoveItemFromParent);
            }
        }

        private void RemoveItemFromParent()
        {
            Panel parentPanel = (Panel)this.Parent;
            parentPanel.Children.Remove(this);
            Device.Connection.Disconnected -= Connection_Disconnected;
        }

        private void SetDeviceNameText(string name)
        {
            deviceNameText.Text = name;
        }

        private void Connection_Authenticated(object source, AuthenticatedEventArgs e)
        {
            Device.Connection.Authenticated -= Connection_Authenticated;
            Dispatcher.Invoke(() => SetDeviceNameText(e.Identifier));
        }
    }
}
