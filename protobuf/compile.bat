@echo off
echo Starting C# compiler...
protoc.exe -I="../Remo/Service/Protocol" --csharp_out="../Remo/Service/Protocol" "../Remo/Service/Protocol/remo.proto"
echo Starting Java compiler
protoc.exe -I="../Remo/Service/Protocol" --java_out="D:\Projects\ADT\Remo\app\src\main\java" "../Remo/Service/Protocol/remo.proto"
pause