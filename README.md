# README #
[![Build status](https://ci.appveyor.com/api/projects/status/m9mf4jw0iwiwhu2j/branch/master?svg=true)](https://ci.appveyor.com/project/FabioGNR/remo-windowsserver/branch/master)  

This is a Remo host application for a Windows PC.

### What does it do? ###

* Provide Controls that other devices can interact with
* Authenticate connecting devices using a random token (reset-able by user)
* Show connected devices and active controls in UI
* Show QR code to allow other devices to easily connect