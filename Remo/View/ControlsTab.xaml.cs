﻿using MahApps.Metro.Controls;
using Remo.Service;

namespace Remo.View
{
    /// <summary>
    /// Interaction logic for ControlsTab.xaml
    /// </summary>
    public partial class ControlsTab : MetroTabItem
    {
        public ControlsTab()
        {
            InitializeComponent();
            PopulateControlsList();
        }

        private void PopulateControlsList()
        {
            foreach (Control control in Controller.Instance.Controls.Values)
            {
                RemoControlItem item = new RemoControlItem(control);
                controlsContainerPanel.Children.Add(item);
            }
        }
    }
}
