﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Google.Protobuf;

namespace Remo.Service
{
    static class ConnectionManager
    {
        public static IReadOnlyList<Device> Devices => ConnectedDevices.AsReadOnly();
        static List<Device> ConnectedDevices { get; } = new List<Device>();
        static TcpListener Listener { get; set; }
        static Thread ListenThread { get; set; }
        private static bool run = false;
        public delegate void DeviceConnectHandler(Device newDevice);
        public static event DeviceConnectHandler Connect;

        public static void Initialize(int port = 5050)
        {
            run = true;   
            ListenThread = new Thread(() => Listen(port));
            ListenThread.Name = "Service listening thread";
            ListenThread.Start();
        }

        public static void Shutdown()
        {
            run = false;
            foreach (var device in ConnectedDevices)
            {
                device.Disconnect();
            }
        }

        public static string GetIpAddress()
        {
            if(Listener != null)
            {
                var endpoint = Listener.Server.LocalEndPoint as IPEndPoint;
                return endpoint.Address.ToString();
            }
            return null;
        }

        static void Listen(int port)
        {
            Listener = new TcpListener(IPAddress.Any, port);
            Listener.Start();
            while (run)
            {
                if (Listener.Pending())
                {
                    var client = Listener.AcceptTcpClient();
                    ConnectToClient(client);
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
            Listener.Stop();
        }

        static void ConnectToClient(TcpClient client)
        {
            Device device = new Device();
            device.Connect(client);
            ConnectedDevices.Add(device);
            Connect?.Invoke(device);
        }

        public static void WriteMessage(this TcpClient client, IMessage msg)
        {
            msg.WriteDelimitedTo(client.GetStream());
        }

        public static T ReadMessage<T> (this TcpClient client) where T : IMessage<T>
        {
            try
            {
                MessageParser<T> parser = (MessageParser<T>)typeof(T).GetProperty("Parser").GetValue(null);
                return parser.ParseDelimitedFrom(client.GetStream());
            }
            catch(Exception)
            {
                return default(T);
            }
        }

        public static bool Authenticate(string token)
        {
            return token == Properties.Settings.Default.AuthenticationToken;
        }
    }
}
