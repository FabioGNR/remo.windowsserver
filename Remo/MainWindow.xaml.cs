﻿using MahApps.Metro.Controls;
using Newtonsoft.Json;
using QRCoder;
using Remo.Service;
using Remo.View;
using System;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Remo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private ImageSource wpfIcon;

        public MainWindow()
        {
            InitializeComponent();
            wpfIcon = Properties.Resources.Icon.ToBitmap().ToImageSource();
            if(wpfIcon != null)
                this.Icon = wpfIcon;
            RefreshTokenText();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Collapsed;
        }

        private void GenerateQRImage()
        {
            object deviceInfo = new
            {
                ipAddress = NetworkUtil.GetIpAddress(),
                token = Properties.Settings.Default.AuthenticationToken,
                macAddress = NetworkUtil.GetMacAddress()
            };
            string qrContent = JsonConvert.SerializeObject(deviceInfo);
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrContent, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20,
                System.Drawing.Color.Black,
                System.Drawing.Color.White,
                Properties.Resources.Icon.ToBitmap(), 15, 20);
            qrCodeBox.Fill = new ImageBrush(qrCodeImage.ToImageSource());
        }
        
        private void RefreshTokenText()
        {
            string token = Properties.Settings.Default.AuthenticationToken;
            authenticationTokenBox.Text = token;
            GenerateQRImage();
        }

        private void TokenButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshTokenText();
            tokenPopup.IsOpen = true;
        }

        private void RefreshTokenButton_Click(object sender, RoutedEventArgs e)
        {
            Authenticator.RefreshToken();
            RefreshTokenText();
        }
    }
}
