﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Remo.Service
{
    public enum ControlType
    {
        Unknown, Toggle, NumericRange, NumericValue, String, Action, 
    }

    public abstract class Control
    {
        protected byte[] EmptyValue { get; } = new byte[0];

        public ControlType Type
        {
            get
            {
                Type type = GetType();
                var attr = type.GetCustomAttribute<ControlTypeAttribute>();
                return attr?.Type ?? ControlType.Unknown;
            }
        }
        public string Name {
            get
            {
                Type type = GetType();
                var attr = type.GetCustomAttribute<DisplayNameAttribute>();
                return attr?.DisplayName;
            }
        }
        public string Description {
            get
            {
                Type type = GetType();
                var attr = type.GetCustomAttribute<DescriptionAttribute>();
                return attr?.Description;
            }
        }

        public delegate void UpdateHandler(object sender, DeviceEventArgs e);
        public event UpdateHandler Updated;

        public DateTime? LastUse { get; protected set; } = null;

        public virtual void Set(byte[] value, Device setter)
        {
            LastUse = DateTime.Now;
            Updated?.Invoke(this, new DeviceEventArgs(setter));
        }
        public abstract byte[] Get();

        public virtual void Stop() { }
    }
}
