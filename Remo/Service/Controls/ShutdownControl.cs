﻿using System.ComponentModel;
using System.Diagnostics;

namespace Remo.Service.Controls
{
    [DisplayName("Shutdown"), Description("Shuts down the hosting device"), ControlType(ControlType.Action)]
    class ShutdownControl : Control
    {
        public override void Set(byte[] value, Device setter)
        {
            base.Set(value, setter);
            var psi = new ProcessStartInfo("shutdown", "/s /t 0")
            {
                CreateNoWindow = true,
                UseShellExecute = false
            };
            Process.Start(psi);
        }

        public override byte[] Get()
        {
            return EmptyValue;
        }
    }
}
