﻿using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Controls;
using Ookii.Dialogs.Wpf;

namespace Remo.View
{
    /// <summary>
    /// Interaction logic for SettingsTab.xaml
    /// </summary>
    public partial class SettingsTab : MetroTabItem
    {
        public SettingsTab()
        {
            InitializeComponent();
            LoadSettings();
        }

        private void LoadSettings()
        {
            fileBrowserPathText.Text = Properties.Settings.Default.FileBrowserDefaultPath ?? "C:\\";
        }

        private void fileBrowserPathText_TextChanged(object sender, TextChangedEventArgs e)
        {
            Properties.Settings.Default.FileBrowserDefaultPath = fileBrowserPathText.Text;
            Properties.Settings.Default.Save();
        }

        private void fileBrowserPathBrowse_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.Description = "Select the default directory to be used for the File Browser control.";
            string path = Properties.Settings.Default.FileBrowserDefaultPath;
            if (!string.IsNullOrWhiteSpace(path))
                dialog.SelectedPath = path;
            if (dialog.ShowDialog() ?? false)
            {
                fileBrowserPathText.Text = dialog.SelectedPath;
            }
        }
    }
}
