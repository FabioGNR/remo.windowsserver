﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remo.Service
{
    public static class Authenticator
    {
        private const string TOKEN_CHARACTERS = "abcdefghijklmnopqrstuvwxyz1234567890";
        private const int TOKEN_LENGTH = 12;
        public static string GetToken()
        {
            return Properties.Settings.Default.AuthenticationToken;
        }

        public static void SetToken(string newToken)
        {
            Properties.Settings.Default.AuthenticationToken = newToken;
            Properties.Settings.Default.Save();
        }

        public static void RefreshToken()
        {
            string token = GenerateNewToken();
            SetToken(token);
        }

        private static string GenerateNewToken()
        {
            StringBuilder tokenBuilder = new StringBuilder();
            Random random = new Random();
            for(int i = 0; i < TOKEN_LENGTH; i++)
            {
                int characterIndex = random.Next(0, TOKEN_CHARACTERS.Length-1);
                char character = TOKEN_CHARACTERS[characterIndex];
                if (random.Next(100) > 50)
                    character = char.ToUpper(character);
                tokenBuilder.Append(character);
            }
            return tokenBuilder.ToString();
        }
    }
}
