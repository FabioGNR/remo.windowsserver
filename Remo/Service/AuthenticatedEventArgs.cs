﻿using System;

namespace Remo.Service
{
    public class AuthenticatedEventArgs : EventArgs
    {
        public string Identifier { get; set; }
        public AuthenticatedEventArgs(string identifier)
        {
            Identifier = identifier;
        }
    }
}
