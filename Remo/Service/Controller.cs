﻿using System;
using System.Collections.Generic;
using Remo.Service.Controls;

namespace Remo.Service
{
    class Controller
    {
        static Controller instance = null;
        public static Controller Instance
        {
            get
            {
                if (instance == null)
                    instance = new Controller();
                return instance;
            }
        }
        public Dictionary<string, Control> Controls { get; protected set; } = new Dictionary<string, Control>();

        public Controller()
        {
            AddControl(new VolumeControl());
            AddControl(new AudioDeviceControl());
            AddControl(new ShutdownControl());
            AddControl(new KeyboardControl());
            AddControl(new FileBrowserControl());
            AddControl(new ApplicationControl());
        }

        private void AddControl(Control control)
        {
            Controls.Add(control.Name, control);
        }

        public void Stop()
        {
            foreach (var pair in Controls)
            {
                pair.Value.Stop();
            }
        }

        private Control GetControl(string controlName)
        {
            if (!Controls.ContainsKey(controlName))
                throw new Exception($"Control {controlName} not found.");
            Control control = Controls[controlName];
            return control;
        }

        public void SetControlValue(string controlName, byte[] value, Device setter)
        {
            Control control = GetControl(controlName);
            if (control != null)
            {
                control.Set(value, setter);
            }
        } 

        public byte[] GetControlValue(string controlName)
        {
            Control control = GetControl(controlName);
            if (control != null)
            {
                return control.Get();
            }
            return new byte[0];
        }
    }
}
