﻿using System;
using System.Net.Sockets;

namespace Remo.Service
{
    public class Device
    { 
        public DeviceConnection Connection { get; protected set; }
        public string Identifier { get; protected set; }

        public void Connect(TcpClient client)
        {
            Connection = new DeviceConnection(client, this);
            Connection.Authenticated += Connection_Authenticated;
        }

        private void Connection_Authenticated(object source, AuthenticatedEventArgs e)
        {
            Identifier = e.Identifier;
        }

        public void Disconnect()
        {
            Connection.Disconnect();
        }
    }
}
