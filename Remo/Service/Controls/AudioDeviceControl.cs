﻿using System;
using NAudio.CoreAudioApi;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Text;

namespace Remo.Service.Controls
{
    [DisplayName("Audio Output Device"), Description("Switch between audio devices"), ControlType(ControlType.Toggle)]
    class AudioDeviceControl : Control, NAudio.CoreAudioApi.Interfaces.IMMNotificationClient
    {
        private static ThreadWorker worker;
        MMDevice device1;
        MMDevice device2;
        static MMDeviceEnumerator enumerator;
        PolicyConfigClient client;
        bool state = false;
        string currentDeviceName = string.Empty;

        public AudioDeviceControl()
        {
            worker = new ThreadWorker("Audio Device");
            worker.Execute(InitializeDependencies); 
        }

        public override void Stop()
        {
            worker.Stop();
        }

        private void InitializeDependencies()
        {
            enumerator = new MMDeviceEnumerator();
            client = new PolicyConfigClient();
            var devices = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
            device1 = devices[0];
            device2 = devices.Count > 1 ? devices[1] : device1;
            if (device2.ID == enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console | Role.Multimedia).ID)
            {
                state = true;
                currentDeviceName = device2.FriendlyName;
            }
            else
            {
                currentDeviceName = device1.FriendlyName;
            }
        }

        public override byte[] Get()
        {
            var nameBytes = Encoding.UTF8.GetBytes(currentDeviceName);
            var valueBytes = BitConverter.GetBytes(state);
            long stateLength = valueBytes.LongLength;
            Array.Resize(ref valueBytes, valueBytes.Length + nameBytes.Length);
            Array.Copy(nameBytes, 0, valueBytes, stateLength, nameBytes.Length);
            return valueBytes;
        }

        public void OnDefaultDeviceChanged(DataFlow flow, Role role, string ID) { }
        public void OnDeviceAdded(string ID)
        {
            if(!worker.UsingWorker)
            {
                worker.Execute(() => OnDeviceAdded(ID));
                return;
            }
            if (device1.ID == device2.ID)
            {
                device2 = enumerator.GetDevice(ID);
                var defaultDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console | Role.Multimedia);
                state = device2.ID == defaultDevice.ID;
            }
        }
        public void OnDeviceRemoved(string ID)
        {
            if (!worker.UsingWorker)
            {
                worker.Execute(() => OnDeviceRemoved(ID));
                return;
            }
            if (ID == device2.ID)
                device2 = device1;
            else if (ID == device1.ID)
                device1 = device2;
        }
        public void OnDeviceStateChanged(string ID, DeviceState State) { }
        public void OnPropertyValueChanged(string ID, PropertyKey Key) { }

        public override void Set(byte[] value, Device setter)
        {
            base.Set(null, setter);
            worker.Execute(Toggle);
        }

        private void Toggle()
        {
            if (device1.ID == device2.ID)
                return;
            state = !state;
            var device = state ? device2 : device1;
            string deviceID = device.ID;
            client.SetDefaultEndpoint(deviceID, ERole.eConsole | ERole.eMultimedia);
            currentDeviceName = device.FriendlyName;
        }

        public enum ERole
        {
            eConsole = 0,
            eMultimedia = 1,
            eCommunications = 2,
            ERole_enum_count = 3
        }

        [ComImport, Guid("870AF99C-171D-4F9E-AF0D-E63DF40C2BC9")]
        internal class _PolicyConfigClient
        {
        }

        [Guid("F8679F50-850A-41CF-9C72-430F290290C8"),
            InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        internal interface IPolicyConfig
        {
            [PreserveSig]
            int GetMixFormat(string pszDeviceName, IntPtr ppFormat);

            [PreserveSig]
            int GetDeviceFormat(string pszDeviceName, bool bDefault, IntPtr ppFormat);

            [PreserveSig]
            int ResetDeviceFormat(string pszDeviceName);

            [PreserveSig]
            int SetDeviceFormat(string pszDeviceName, IntPtr pEndpointFormat, IntPtr MixFormat);

            [PreserveSig]
            int GetProcessingPeriod(string pszDeviceName, bool bDefault, IntPtr pmftDefaultPeriod, IntPtr pmftMinimumPeriod);

            [PreserveSig]
            int SetProcessingPeriod(string pszDeviceName, IntPtr pmftPeriod);

            [PreserveSig]
            int GetShareMode(string pszDeviceName, IntPtr pMode);

            [PreserveSig]
            int SetShareMode(string pszDeviceName, IntPtr mode);

            [PreserveSig]
            int GetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetDefaultEndpoint(string pszDeviceName, ERole role);

            [PreserveSig]
            int SetEndpointVisibility(string pszDeviceName, bool bVisible);
        }
        [Guid("568B9108-44BF-40B4-9006-86AFE5B5A620"),
            InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        internal interface IPolicyConfigVista
        {
            [PreserveSig]
            int GetMixFormat(string pszDeviceName, IntPtr ppFormat);

            [PreserveSig]
            int GetDeviceFormat(string pszDeviceName, bool bDefault, IntPtr ppFormat);

            [PreserveSig]
            int ResetDeviceFormat(string pszDeviceName);

            [PreserveSig]
            int SetDeviceFormat(string pszDeviceName, IntPtr pEndpointFormat, IntPtr MixFormat);

            [PreserveSig]
            int GetProcessingPeriod(string pszDeviceName, bool bDefault, IntPtr pmftDefaultPeriod, IntPtr pmftMinimumPeriod);

            [PreserveSig]
            int SetProcessingPeriod(string pszDeviceName, IntPtr pmftPeriod);

            [PreserveSig]
            int GetShareMode(string pszDeviceName, IntPtr pMode);

            [PreserveSig]
            int SetShareMode(string pszDeviceName, IntPtr mode);

            [PreserveSig]
            int GetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetDefaultEndpoint(string pszDeviceName, ERole role);

            [PreserveSig]
            int SetEndpointVisibility(string pszDeviceName, bool bVisible);
        }
        [Guid("00000000-0000-0000-C000-000000000046"),
            InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        internal interface IPolicyConfig10
        {
            [PreserveSig]
            int GetMixFormat(string pszDeviceName, IntPtr ppFormat);

            [PreserveSig]
            int GetDeviceFormat(string pszDeviceName, bool bDefault, IntPtr ppFormat);

            [PreserveSig]
            int ResetDeviceFormat(string pszDeviceName);

            [PreserveSig]
            int SetDeviceFormat(string pszDeviceName, IntPtr pEndpointFormat, IntPtr MixFormat);

            [PreserveSig]
            int GetProcessingPeriod(string pszDeviceName, bool bDefault, IntPtr pmftDefaultPeriod, IntPtr pmftMinimumPeriod);

            [PreserveSig]
            int SetProcessingPeriod(string pszDeviceName, IntPtr pmftPeriod);

            [PreserveSig]
            int GetShareMode(string pszDeviceName, IntPtr pMode);

            [PreserveSig]
            int SetShareMode(string pszDeviceName, IntPtr mode);

            [PreserveSig]
            int GetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetPropertyValue(string pszDeviceName, bool bFxStore, IntPtr key, IntPtr pv);

            [PreserveSig]
            int SetDefaultEndpoint(string pszDeviceName, ERole role);

            [PreserveSig]
            int SetEndpointVisibility(string pszDeviceName, bool bVisible);
        }
        public class PolicyConfigClient
        {
            private readonly IPolicyConfig _PolicyConfig;
            private readonly IPolicyConfigVista _PolicyConfigVista;
            private readonly IPolicyConfig10 _PolicyConfig10;

            public PolicyConfigClient()
            {
                _PolicyConfig = new _PolicyConfigClient() as IPolicyConfig;
                if (_PolicyConfig != null)
                    return;

                _PolicyConfigVista = new _PolicyConfigClient() as IPolicyConfigVista;
                if (_PolicyConfigVista != null)
                    return;

                _PolicyConfig10 = new _PolicyConfigClient() as IPolicyConfig10;
            }

            public void SetDefaultEndpoint(string devID, ERole eRole)
            {
                if (_PolicyConfig != null)
                {
                    Marshal.ThrowExceptionForHR(_PolicyConfig.SetDefaultEndpoint(devID, eRole));
                    return;
                }
                if (_PolicyConfigVista != null)
                {
                    Marshal.ThrowExceptionForHR(_PolicyConfigVista.SetDefaultEndpoint(devID, eRole));
                    return;
                }
                if (_PolicyConfig10 != null)
                {
                    Marshal.ThrowExceptionForHR(_PolicyConfig10.SetDefaultEndpoint(devID, eRole));
                }
            }
        }
    }
}
