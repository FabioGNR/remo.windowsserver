﻿using Remo.Service;
using System.Windows;
using Hardcodet.Wpf.TaskbarNotification;
using System.Windows.Controls;

namespace Remo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindow mainWindow;
        private TaskbarIcon trayIcon;
        private const string CLOSE_MENU_ITEM = "Close";

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ConnectionManager.Initialize();

            mainWindow = new MainWindow();
            CreateTrayIcon();
        }

        private void CreateTrayIcon()
        {
            var contextMenu = new ContextMenu();
            var closeItem = new MenuItem();
            closeItem.Header = CLOSE_MENU_ITEM;
            closeItem.Click += CloseItem_Click;
            contextMenu.Items.Add(closeItem);
            trayIcon = new TaskbarIcon
            {
                ToolTipText = "Remo Server",
                Visibility = Visibility.Visible,
                Icon = Remo.Properties.Resources.Icon,
                ContextMenu = contextMenu
            };
            trayIcon.TrayMouseDoubleClick += TrayIcon_TrayMouseDoubleClick;
        }

        private void CloseTrayIcon()
        {
            trayIcon.Visibility = Visibility.Collapsed;
            trayIcon.Dispose();
            trayIcon = null;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            CloseTrayIcon();
            base.OnExit(e);
            ConnectionManager.Shutdown();
            Controller.Instance.Stop();
        }

        private void CloseItem_Click(object sender, RoutedEventArgs e)
        {
            Shutdown();
        }

        private void TrayIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            mainWindow.Show();
        }
    }
}
