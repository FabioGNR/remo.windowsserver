﻿using System;

namespace Remo.Service
{
    class ControlTypeAttribute : Attribute
    {
        public ControlType Type { get; private set; }
        public ControlTypeAttribute(ControlType type)
        {
            this.Type = type;
        }
    }
}
