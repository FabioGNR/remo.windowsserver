﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace Remo.Service.Controls
{
    [DisplayName("Application Launcher"), Description("Launch Kodi"), ControlType(ControlType.Action)]
    class ApplicationControl : Control
    {
        const string KodiPath = @"C:\Users\Fabio\Documents\Rainmeter\Skins\Icon Dock\@Resources\Shortcuts\common\Kodi.lnk";
        public override void Set(byte[] value, Device setter)
        {
            base.Set(value, setter);
            if (File.Exists(KodiPath))
            {
                // start process
                var process = Process.Start(KodiPath);
                // attempt to focus window
                var windowHandle = process.MainWindowHandle;
                if(windowHandle != IntPtr.Zero)
                {
                    NativeMethods.ShowWindow(windowHandle, 5); // 5 = SW_SHOW, 9 = SW_RESTORE
                }
            }
        }

        public override byte[] Get()
        {
            return EmptyValue;
        }
    }
}
