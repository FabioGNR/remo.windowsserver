﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Remo.View
{
    /// <summary>
    /// Interaction logic for RemoControlItem.xaml
    /// </summary>
    public partial class RemoControlItem : UserControl
    {
        private readonly Service.Control Control;

        public RemoControlItem(Service.Control control)
        {
            this.Control = control;
            InitializeComponent();
            controlNameText.Text = control.Name;
            controlNameDescription.Text = control.Description;
            RefreshLastUsedText();
            IsVisibleChanged += RemoControlItem_IsVisibleChanged;
        }

        private void RemoControlItem_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue && !(bool)e.OldValue)
            {
                Control.Updated += Control_Updated;
                RefreshLastUsedText();
            }
            else if(!(bool)e.NewValue && (bool)e.OldValue)
            {
                Control.Updated -= Control_Updated;
            }
        }

        private void Control_Updated(object source, EventArgs e)
        {
            this.Dispatcher.Invoke(() => RefreshLastUsedText());
        }

        private void RefreshLastUsedText()
        {
            string usedText = string.Empty;
            if (Control.LastUse != null)
            {
                usedText = "Last used " + Control.LastUse.Value.ToLongTimeString();
            }
            else
            {
                usedText = "Last used never";
            }
            lastUsedText.Text = usedText;
        }
    }
}
