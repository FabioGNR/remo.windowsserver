﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Remo.Service.Controls
{
    /// <summary>
    /// Executes code on single thread so singlethread APIs can be used in the multithreaded Control context
    /// </summary>
    class ThreadWorker : IDisposable
    {
        public delegate void WorkItem();
        public bool UsingWorker => Thread.CurrentThread == thread;
        public AutoResetEvent Reset = new AutoResetEvent(false);
        private AutoResetEvent RunReset = new AutoResetEvent(false);
        private Queue<WorkItem> WorkQueue = new Queue<WorkItem>();
        private Thread thread;
        private bool running = true;

        public ThreadWorker(string controlName)
        {
            thread = new Thread(new ThreadStart(() => Run()))
            {
                Name = controlName + " Thread Worker"
            };
            thread.Start();
        }

        public void Stop()
        {
            running = false;
        }

        public void Execute(WorkItem item)
        {
            WorkQueue.Enqueue(item);
            RunReset.Set();
            Reset.WaitOne();
        }

        private void Run()
        {
            while (running)
            {
                RunReset.WaitOne(500);
                if (WorkQueue.Count > 0)
                {
                    WorkItem item = WorkQueue.Dequeue();
                    item();
                    Reset.Set();
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Reset.Dispose();
                    RunReset.Dispose();
                }

                thread = null;

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
