﻿using System;

namespace Remo.Service
{
    public class DeviceEventArgs : EventArgs
    {
        public Device Device { get; private set; }
        public DeviceEventArgs(Device device)
        {
            Device = device;
        }
    }
}
