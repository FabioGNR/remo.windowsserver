﻿using MahApps.Metro.Controls;
using Remo.Service;

namespace Remo.View
{
    /// <summary>
    /// Interaction logic for DevicesTab.xaml
    /// </summary>
    public partial class DevicesTab : MetroTabItem
    {
        public DevicesTab()
        {
            InitializeComponent();
            ConnectionManager.Connect += ConnectionManager_Connect;
            PopulateDevicesList();
        }

        private void ConnectionManager_Connect(Device newDevice)
        {
            Dispatcher.Invoke(() => AddDevice(newDevice));
        }

        private void AddDevice(Device device)
        {
            DeviceItem item = new DeviceItem(device);
            devicesContainer.Children.Add(item);
        }

        public void PopulateDevicesList()
        {
            var devices = ConnectionManager.Devices;
            foreach (var device in devices)
            {
                AddDevice(device);
            }
        }
    }
}
